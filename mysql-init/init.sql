USE zepdb;

CREATE TABLE IF NOT EXISTS  BankInfo (
    age int(9) NOT NULL,
    job enum('admin.','unknown','unemployed','management','housemaid','entrepreneur','student', 'blue-collar','self-employed','retired','technician','services') COMMENT 'ype of job as categorical',
    marital enum('married','divorced') COMMENT 'note: divorced means divorced or widowed' ,
    education enum ('unknown','secondary','primary','tertiary'),
    default_credit enum ('yes', 'no') COMMENT 'has credit in default? (binary: yes, no)',
    balance  int(9) NOT NULL COMMENT 'average yearly balance, in euros',
    housing enum ('yes', 'no') COMMENT 'has housing loan?',
    loan enum ('yes', 'no') COMMENT 'has personal loan?',
    contact enum ('unknown','telephone','cellular') COMMENT 'contact communication type',
    day int(3) NOT NULL COMMENT 'last contact day of the month', 
    month enum ('jan','feb','mar','apr','nay','jun','jul','aug','sep','oct','nov','dec') COMMENT 'last contact month of year', 
    duration int(9) NOT NULL COMMENT 'last contact duration, in seconds',
    campaign int(9) COMMENT 'number of contacts performed during this campaign and for this client (numeric, includes last contact)', 
    pdays int(9) COMMENT 'number of days that passed by after the client was last contacted from a previous campaign (numeric, -1 means client was not previously contacted)', 
    previous int(9) COMMENT 'number of contacts performed before this campaign and for this client', 
    poutcome enum ('unknown','other','failure','success') COMMENT 'outcome of the previous marketing campaign',
    subscribed_term_deposit enum ('yes', 'no') COMMENT  'has the client subscribed a term deposit?'
);

CREATE TABLE IF NOT EXISTS Tasks (
    task_id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    start_date DATE,
    due_date DATE,
    status TINYINT NOT NULL,
    priority TINYINT NOT NULL,
    description TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);